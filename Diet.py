# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 19:45:17 2020

@author: Mauro
"""

from gurobipy import *

import os
import xlrd

book = xlrd.open_workbook(os.path.join("Diet.xlsx"))
sh = book.sheet_by_name("Table")

nutrients = []
foods = []
#reads the nutrients (labels)
i = 2
while True:
    try:
        c = sh.cell_value(i, 0)
        nutrients.append(c)
        i = i + 1
    except IndexError:
        break
row_cost = i-1
del nutrients[-1] #remove the cost
print ("Nutrients = ", nutrients)
#reads the foodss (labels)
i = 1
while True:
    try:
        c = sh.cell_value(1, i)
        foods.append(c)
        i = i + 1
    except IndexError:
        break
del foods[-1] #removes the last three labels 
del foods[-1]
del foods[-1]
print ("Foods = ", foods)
#reads the costs
costs = {f : 0 for f in foods}
for f in foods:
   c = sh.cell_value(row_cost, foods.index(f)+1)
   costs[f] = c

#print (costs)

#reads matrix of nutrients for each food
A = {(n,f) : 0 for n in nutrients for f in foods}
for n in nutrients:
    for f in foods:
       c = sh.cell_value(nutrients.index(n)+2, foods.index(f)+1)
       A[n,f] = c
#print (Matrix)

#reads requirements
MinRequirement = {n : 0 for n in nutrients}
MaxRequirement = {n : 0 for n in nutrients}
for n in nutrients:
    c = sh.cell_value(nutrients.index(n)+2, len(foods)+1)
    MinRequirement[n] = c
    c = sh.cell_value(nutrients.index(n)+2, len(foods)+2)
    MaxRequirement[n] = c
       
###################
m = Model("Diet1")

x = m.addVars(foods,vtype=GRB.CONTINUOUS,name=foods)

m.addConstrs( (quicksum(A[n,f] * x[f] for f in foods)  >= MinRequirement[n])   for n in nutrients)

m.setObjective(quicksum(costs[f] * x[f] for f in foods) , GRB.MINIMIZE)

m.optimize()

m.write("Diet.lp")

print('\nCost: %g\n' % m.objVal)
        
for i in m.getVars():
    print(i.varName, i.x)

print()    
for f in foods: 
   if x[f].x > 0.0001:
        print ('%16s = %f' % (f,x[f].x) )
