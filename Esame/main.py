import math
import sys
import networkx as nx
from Truck import Truck
import matplotlib.pyplot as plt
from gurobipy import *
from Master import Master
from Subproblem import Subproblem
from compactModel import CompactModel


def compute_distance_matrix(customers_x, customers_y):
    n_customers = len(customers_x)
    # funzione che calcola la matrice delle distanze
    distance_matrix = [[None for i in range(n_customers)] for j in range(n_customers)]

    for i in range(n_customers):
        distance_matrix[i][i] = 0
        for j in range(n_customers):
            dist = compute_eucledian_distance(customers_x[i], customers_x[j], customers_y[i], customers_y[j])
            distance_matrix[i][j] = dist
            distance_matrix[j][i] = dist
    return distance_matrix


def compute_eucledian_distance(xi, xj, yi, yj):
    # funzione alla quale dati due punti x e y, calcola la distanza euclidea tra i questi due
    exact_dist = math.sqrt(math.pow(xi - xj, 2) + math.pow(yi - yj, 2))
    return int(math.floor(exact_dist + 0.5))


def compute_total_distance_traveled(trucks):
    # funzione per il calcolo della distanza totale percorsa dai trucks
    tot = 0
    for t in trucks:
        tot += t.distance_traveled
    return tot

def get_routes(model):
    routes = []
    if model.r.status == GRB.OPTIMAL:
        k_ = 0
        for v in model.r.getVars():
            if v.varName == "Y0" or v.varName == "Yc":
                continue
            if v.x != 0:
                #print(v.varName, ":", v.x, end=' ----------- ')
                import re

                index = int(re.findall(r'\d+', v.varName)[0])
                routes.append(createRoute(A, index, S_M))
                #print_route(routes[k_], demands, Q, label=k_)
                k_ += 1
    return routes

def check_routes(A, routes, customers):
    # check the correctness of the routes
    list = []
    for route in routes:
        for client in route:
            list.append(client[0])
    list.sort()
    check_list = [i for i in range(1, len(customers))]
    if list == check_list:
        return True
    else:
        print("lista per il debug:")
        print(list)
        print("come dovrebbe essere:")
        print(check_list)

        print("------------ Matrice A --------")
        for i in range(len(A)):
            for j in range(len(A[0])):
                print(A[i][j], "<-[" + str(j) + "]", end=' ')
            print()
        return False


def print_model_variables(A, S_M, model, demands, Q):
    routes = []
    if model.r.status == GRB.OPTIMAL:
        k_ = 0
        for v in model.r.getVars():
            if v.varName == "Y0" or v.varName == "Yc":
                continue
            if v.x != 0:
                print(v.varName, ":", v.x, end=' ----------- ')
                import re

                index = int(re.findall(r'\d+', v.varName)[0])
                routes.append(createRoute(A, index, S_M))
                print_route(routes[k_], demands, Q, label=k_)
                k_ += 1


def plot_dataset(data_x, data_y):
    # funzione per il debugging, plotta i nodi in un grafico
    plt.scatter(data_x, data_y)

    labels = [i for i in range(len(customers_x) + 1)]
    for x, y, label in zip(data_x, data_y, labels):
        plt.annotate(label, xy=(x, y))
    plt.show()

    def create_graph(trucks, customers_x, customers_y, depot_x, depot_y):
        # funzione che va a disegnare il grafo finale del problema risolto con la local search.
        G = nx.Graph()
        for i in range(len(customers_x) + 1):
            G.add_node(i)

        for t in trucks:
            i = 0
            j = 1
            while j < len(t.route):
                G.add_edge(t.route[i], t.route[j], len=distance_matrix)
                i += 1
                j += 1

        data_x = []
        data_y = []
        data_x.append(depot_x)
        data_x += customers_x
        data_y.append(depot_y)
        data_y += customers_y

        pos = {i: 0 for i in range(len(data_x))}
        i = 0
        for x, y in zip(data_x, data_y):
            pos[i] = [x, y]
            i += 1

        nx.draw_networkx_nodes(G, pos=pos)
        nx.draw_networkx_labels(G, pos=pos)
        import numpy as np
        for t in trucks:
            color = (np.random.uniform(0, 1), np.random.uniform(0, 1), np.random.uniform(0, 1), 1)
            edgelist = []
            i = 0
            j = 1
            while j < len(t.route):
                tmp = (t.route[i], t.route[j])
                edgelist.append(tmp)
                i += 1
                j += 1
            nx.draw_networkx_edges(G, edgelist=edgelist, edge_color=color, pos=pos)

        # nx.draw(G, with_labels=True, pos=pos, edge_color=c)
        plt.show()


def generate_initial_solution(customers_x):
    # creo tanti paths quanti clienti ho, per ogni path servo un cliente differente
    # questa è una soluzione iniziale ammissibile, anche se sicuramente non sarà ottima
    matrix = [[0 for i in range(len(customers_x))] for j in range(len(customers_x))]
    for i in range(len(customers_x)):
        for j in range(len(customers_x)):
            if i == 0:
                matrix[i][j] = 1
            if i == j:
                matrix[i][j] = 1
    return matrix


def extract_feasable_customer(customers, q, actual_time_vehicle, distance_matrix, last_client_served):
    # sto ragionando come se fossi un veicolo k
    if len(customers) == 0:
        return -2, None

    # se mi trovo al deposito e parto dal deposito al tempo zero, poi mando il camion al primo cliente e lo faccio aspettare
    # l'attesa in questo modello non viene penalizzata, il tempo di partenza del camion corrisponderà quindi al tempo
    # relativo nella time window del primo cliente servito

    # se mi trovo nel deposito
    if last_client_served == 0:
        for cust in customers:
            # se il veicolo per andare dal cliente ci impiega di più della prima time window del cliente, allora
            # il tempo del veicolo è pari alla distanza, quindi è il tempo di viaggio per raggiungere il cliente
            if (actual_time_vehicle + distance_matrix[last_client_served][cust.index]) >= cust.time_window[0]:
                actual_time_vehicle = actual_time_vehicle + distance_matrix[last_client_served][cust.index]
                return cust, actual_time_vehicle
            # altrimenti il veicolo arriva dal cliente e aspetta e poi lo serve appena il cliente è pronto, l'attesa
            # non si paga in questo modello
            else:
                actual_time_vehicle = cust.time_window[0]
                return cust, actual_time_vehicle

    i = 0
    for cust in customers:
        # print("Time window cliente {} -> [{},{}]".format(cust.index, cust.time_window[0], cust.time_window[1]))
        # print("Valore da inserire: {}".format(distance_matrix[last_client_served][cust.index] + actual_time_vehicle))
        # print("Ci sta? {}".format(
        #     cust.time_window[0] <= distance_matrix[last_client_served][cust.index] + actual_time_vehicle <= \
        #     cust.time_window[1]))
        if cust.time_window[0] <= distance_matrix[last_client_served][cust.index] + actual_time_vehicle <= \
                cust.time_window[1]:
            if q - cust.demand >= 0:
                customers.pop(i)
                return cust, actual_time_vehicle
        i += 1
    # non ho trovato un customer feasable per il veicolo
    return -1, None


def generate_initial_greedy_solution(customers_x, K, time_windows, service_times, demands, distance_matrix, Q):
    # creo tanti paths quanti clienti ho, per ogni path servo un cliente differente
    # questa è una soluzione iniziale ammissibile, anche se sicuramente non sarà ottima

    # lista delle capacità rimanenti per veicolo
    q = [Q]
    actual_time_vehicle = [0]
    matrix = [[0] for i in range(len(customers_x))]
    matrix[0][0] = 1
    last_client_served = 0
    col = [0 for i in range(len(customers_x))]
    col[0] = 1

    s_matrix = [[-1] for i in range(len(customers_x))]
    starting_times = [-1 for i in range(len(customers_x))]
    starting_times[0] = -1

    class Customer:
        def __init__(self, time_window, service_time, demand, index):
            self.time_window = time_window
            self.service_time = service_time
            self.demand = demand
            self.index = index

    customers = []
    for i in range(1, len(customers_x)):
        customer = Customer(time_windows[i], service_times[i], demands[i], i)
        customers.append(customer)

    customers.sort(key=lambda x: x.time_window)
    # for c in customers:
    #     print(c.time_window)
    #     print(c.index)
    k = 0
    while True:
        # if k >= K:
        #     print("Non ho sufficienti veicoli per creare i percorsi!")
        #     return -1
        # print("k: ", k)
        cust, time_vehicle = extract_feasable_customer(customers, q[k], actual_time_vehicle[k], distance_matrix,
                                                       last_client_served)

        # non ho trovato un customer ammissibile per il veicolo, termino quindi la creazione del percorso
        if cust == -1:
            # print("Aggiungo colonna:")
            # print(col)
            matrix = addColumn(matrix, col)
            s_matrix = addColumn(s_matrix, starting_times)
            # creo una nuova colonna
            col = [0 for i in range(len(customers_x))]
            col[0] = 1
            starting_times = [-1 for i in range(len(customers_x))]
            starting_times[0] = -1
            # aggiungo un veicolo dinamicamente
            k += 1
            q.append(Q)
            actual_time_vehicle.append(0)
            last_client_served = 0
            continue
        # codice di terminazione, non ho più clienti da servire
        if cust == -2:
            matrix = addColumn(matrix, col)
            s_matrix = addColumn(s_matrix, starting_times)
            k += 1
            print("Finiti i clienti da servire")
            print("Utilizzati {} veicoli".format(k))
            break
        # servo il cliente
        # print("Servo cliente: {}".format(cust.index))
        actual_time_vehicle[k] = time_vehicle
        last_client_served = cust.index
        q[k] -= cust.demand
        col[cust.index] = 1
        starting_times[cust.index] = cust.time_window[0]
        # print(col)

    return matrix, s_matrix


def addColumn(A, new_col):
    N = [[0 for j in range(len(A[0]) + 1)] for i in range(len(A))]
    for i in range(len(A)):
        for j in range(len(A[0]) + 1):
            if j == len(A[0]):
                # print("i: {}, j: {}".format(i, j))
                # print("Dimensione di N: {}x{} ".format(len(N), len(N[0])))
                N[i][j] = new_col[i]
                break
            N[i][j] = A[i][j]
    return N


def removeCol(A, col):
    N = [[0 for j in range(len(A[0]) - 1)] for i in range(len(A))]
    for i in range(len(A)):
        k = 0
        for j in range(len(A[0])):
            if j == col:
                continue
            else:
                N[i][k] = A[i][j]
                k += 1
    return N


def print_route(route, demands, Q, label=None):
    print("Route {}:".format(label), end=' ')
    tot_cap = Q
    for client in route:
        print("{}".format(client), end=' ')
        tot_cap -= demands[client[0]]

    print("| Capacità rimasta: ", tot_cap)
    print()


def createRoute(A, index, S_M):
    route = []
    for i in range(1, len(A)):
        if A[i][index] != 0:
            route.append((i, S_M[i][index]))
    route = sorted(route, key=lambda x: x[1])
    return route


def calculate_route_cost(route, distance_matrix):
    tot = 0
    last_visited = 0
    for r in route:
        tot += distance_matrix[last_visited][r]
        last_visited = r
    tot += distance_matrix[last_visited][0]
    return tot


def check_fractional(master):
    fractional = False
    if master.r.status == GRB.OPTIMAL:
        for i in master.r.getVars():
            if i.varName != "Y0" or i.varName != "Yc":
                if 0 < i.x < 1:
                    fractional = True
    return fractional


def modify_graph(subproblem, A, S_M, value, i, j, customers):
    column_to_remove = []
    arcs_removed = []
    if value == 0:
        # print("Devo rimuovere arco {}-{}".format(i,j))
        subproblem.x[i, j] = 0
        subproblem.m.addConstr(subproblem.x[i, j] == 0)
        # rimuovo la colonna se utilizza l'arco
        for j_ in range(len(A[0])):
            col = []
            for i_ in range(len(A)):
                if A[i_][j_] != 0:
                    col.append((i_, S_M[i_][j_]))
            arcs = get_arcs(col)
            # print(arcs)
            for k, l in arcs:
                # print("i: {}, j: {}".format(i,j))
                # print("k: {}, l: {}".format(k,l))
                if i == k and j == l:
                    # print("Arco trovato nella colonna :", j_)
                    column_to_remove.append(j_)

    else:
        for j_ in range(len(customers)):
            if j_ == j:
                subproblem.x[i, j_] = 1
                subproblem.m.addConstr(subproblem.x[i, j_] == 1)
            else:
                # tutti gli archi che vanno da i verso un altro j li elimino
                subproblem.x[i, j_] = 0
                subproblem.m.addConstr(subproblem.x[i, j_] == 0)
                arcs_removed.append((i, j_))
        for i_ in range(len(customers)):
            if i_ != i:
                # tutti gli archi che vanno da un cliente i_ diverso da i a j li elimino
                subproblem.x[i_, j] = 0
                subproblem.m.addConstr(subproblem.x[i_, j] == 0)
                arcs_removed.append((i_, j))
        print("Arcs removed: ", arcs_removed)

        # aggiusto la matrice A eliminando le colonne che utilizzano quegli archi
        # for j in range(len(A[0])):
        #     col = []
        #     for i in range(len(A)):
        #         if A[i][j] != 0:
        #             col.append((i, S_M[i][j]))
        #     arcs = get_arcs(col)
        #     # print(arcs)
        #     for k, l in arcs_removed:
        #         if (k, l) in arcs:
        #             # print("Arco {}-{} trovato nella colonna : {}".format(k,l,j))
        #             column_to_remove.append(j)
        #             break
    count = 0
    for col in column_to_remove:
        # non modifico il master, andrebbe a complicare troppo il codice, rimuovo la colonna da A, ho lo stesso effetto
        # master.m.addConstr(master.y[col] == 0)
        A = removeCol(A, col - count)
        S_M = removeCol(S_M, col - count)
        count += 1

    print("Colonne rimosse: ", column_to_remove)
    print("Numero: ", count)
    return A, S_M, subproblem


def get_arcs(route):
    route = sorted(route, key=lambda x: x[1])
    arcs = []
    i = 0
    for j in range(1, len(route)):
        a = route[i][0]
        b = route[j][0]
        i += 1
        arcs.append((a, b))
    return arcs


def print_matrix(matrix):
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            print(matrix[i][j], end=' ')
        print()


def calculate_route_capacity(route, demands):
    q = 0
    for client in route:
        q += demands[client[0]]
    return q


#filename = "./data/c101.txt"
# filename = "./data/c101_25.txt"
# filename = "./data/c101_30.txt"
#filename = "./data/c101_50.txt"
# filename = "./data/c101_5.txt"

# filename = "./data/r101.txt"
#filename = "./data/r101_30.txt"

filename = "./data/c205.txt"




with open(filename, "r") as f:
    data = [str(elem) for elem in f.read().split()]
    data_iter = iter(data)

instance_name = str(next(data_iter))
# print(data)

customers_x = []
customers_y = []
demands = []
time_windows = []
service_times = []

# estraggo un elemento
el = next(data_iter)

if el == "VEHICLE":
    # mi sposto avanti di altri due token e recupero direttamente il numero di veicoli massimo e la capacità
    # Es -> 'VEHICLE', 'NUMBER', 'CAPACITY', '25', '200'
    next(data_iter)
    next(data_iter)
    K = int(next(data_iter))
    Q = int(next(data_iter))
    print("K: {}, Q: {}".format(K, Q))
# devo rimuovere tutti i nomi delle colonne e poi inizio a recuperare i vari dati
for i in range(12):
    next(data_iter)
# da qui ho tutti i miei dati che devo parsare, il customer 0 è il deposito

while (True):
    try:
        # salto il numero del customer
        next(data_iter)
        customers_x.append(int(next(data_iter)))
        customers_y.append(int(next(data_iter)))
        demands.append(int(next(data_iter)))
        time_windows.append((int(next(data_iter)), int(next(data_iter))))
        service_times.append(int(next(data_iter)))
    except:
        break

print("---- customer x -----")
print(customers_x)
print("---- customer y -----")
print(customers_y)
print("--- demands -----")
print(demands)
print("--- time windows ----")
print(time_windows)
print("---- service times ----")
print(service_times)

print("----- distance matrix ---- ")
distance_matrix = compute_distance_matrix(customers_x, customers_y)
print(distance_matrix)

print("---------------- Compact model -------------------")
compactM = CompactModel(distance_matrix, K, Q, demands, time_windows, service_times, customers_x)
resComp = 0
# resComp = compactM.resolve()
# sys.exit(0)

# plot_dataset(customers_x, customers_y)

# base matrix

# A = generate_initial_solution(customers_x)
# S_M è la starting time matrix
A, S_M = generate_initial_greedy_solution(customers_x, K, time_windows, service_times, demands, distance_matrix, Q)

# errore nella creazione della soluzione greedy iniziale
if A == -1:
    sys.exit(1)

# sys.exit(0)
# for j in range(K):
#     for i in range(len(customers_x)):
#         # if i == 0:
#         #     A[i][j] = 1
#         #     continue
#         if (i-1) == j:
#             A[i][j] = 1


print("---------- Matrice A ------------")
print_matrix(A)
res = -999
iters = 0
old_master_sol = 0
# # if res > 1+(10**(-6)) vuol dire che i costi ridotti sono negativi e sono arrivato all'ottimo
master = Master(A, distance_matrix, K)
master.initModel(K)
j = 0
initial_length = len(A[0]) - 1
while (res < 0):

    master = Master(A, distance_matrix, K)
    # master.setMatrix(A)
    master.initModel(K)
    # myCol = Column()
    # effettuo il cutoff per tagliare la fase di coda

    master_sol = master.resolve()

    u = master.getDualVariables()
    print("var duali: ", u)
    # setto le variabili duali nello slave
    subproblem = Subproblem(distance_matrix, customers_x, Q, demands, time_windows, service_times, u)
    subproblem.m.update()
    # risolvo il sottoproblema
    res = subproblem.resolve()
    # Estraggo la nuova colonna da aggiungere ai vincoli
    new_col = subproblem.getNewCol()
    # print("----------> new col:", new_col)
    # print("constraint: ", master.getConstrs())
    starting_times = subproblem.getStartingTimes()
    print("------- Aggiungo al master la colonna: ------")
    print(new_col)

    A = addColumn(A, new_col)
    S_M = addColumn(S_M, starting_times)
    cost = calculate_route_capacity(createRoute(A, len(A[0]) - 1, S_M), demands)
    print("Costo della nuova colonna in capacità: ", cost)
    print("---------------------")
    # tengo traccia dell'iterazione appena svolta

    # new_col_ = new_col[1:]
    # print("contr master: ", master.m.getConstrs())
    # print("new col len: ", len(new_col_))
    # myCol.addTerms(new_col, master.m.getConstrs())
    master.m.update()

    iters += 1

    # creo la nuova variabile e la aggancio ai nuovi vincoli
    # master.m.addVar(vtype=GRB.BINARY, name="y[" + str(initial_length + iters) + "]", column=myCol, obj=1)
    # effettuo il cutoff per eliminare iterazioni inutili che portano miglioramenti minimi
    if (master_sol - old_master_sol) == 0:
        j += 1
        # se ottengo dopo 20 iterazioni la soluzione continua ad avere miglioramenti minimi esco perchè mi trovo nella coda
        if j == 5:
            break
    else:
        j = 0
    # if iters == 10:
    #     break

    old_master_sol = master_sol

master_sol = master.resolve()
# salvo il tutto
master.m.write("VRPT_master.lp")
subproblem.m.write("VRPT_subproblem.lp")

print("========== Branching a due livelli... ===============")
print("============= level 1 ==================")
branching = False
# controllo se la somma dei veioli è intera o frazionaria
y0 = 0
yc = 0
for i in master.r.getVars():
    if i.varName == "Y0":
        y0 = i.x
    if i.varName == "Yc":
        yc = i.x
y0_before = y0
# print("y0 : ", y0)
import math

ceil_y0 = math.ceil(y0)
floor_y0 = math.floor(y0)
old_y0_ceil = ceil_y0
old_y0_floor = floor_y0
print("original value: ", y0)
print("ceil: ", ceil_y0)
print("floor: ", floor_y0)
print("YC: ", yc)

# se sono diversi vuol dire che la soluzione è frazionaria e faccio il branching

if ceil_y0 != floor_y0:
    branching = True

    master2 = Master(A, distance_matrix, K)
    master2.initModel(K)
    master2.extend_model_level1(ceil_y0, right=True)

    A1 = [[A[i][j] for j in range(len(A[0]))] for i in range(len(A))]
    S_M1 = [[S_M[i][j] for j in range(len(S_M[0]))] for i in range(len(S_M))]
    master_sol1 = 0
    iters1 = 0
    j = 0
    # finchè la y0 non è intera continuo a ciclare
    while (ceil_y0 != floor_y0):
        master1 = Master(A1, distance_matrix, K)
        master1.initModel(K)
        master1.extend_model_level1(floor_y0, left=True)

        # master1.m.computeIIS()
        # master1.m.write("Master1.ilp")
        try:
            master_sol1 = master1.resolve()
        except:
            break
        u = master1.getDualVariables()
        print("var duali: ", u)
        # setto le variabili duali nello slave
        subproblem = Subproblem(distance_matrix, customers_x, Q, demands, time_windows, service_times, u)
        subproblem.m.update()
        # risolvo il sottoproblema
        res = subproblem.resolve()

        # Estraggo la nuova colonna da aggiungere ai vincoli
        new_col = subproblem.getNewCol()
        starting_times = subproblem.getStartingTimes()
        # print("----------> new col:", new_col)
        # print("constraint: ", master.getConstrs())

        print("------- Aggiungo al master la colonna: ------")
        print(new_col)
        A1 = addColumn(A1, new_col)
        S_M1 = addColumn(S_M1, starting_times)
        cost = calculate_route_capacity(createRoute(A1, len(A1[0]) - 1, S_M1), demands)
        print("Costo della nuova colonna in capacità: ", cost)
        print("---------------------")
        # tengo traccia dell'iterazione appena svolta

        # new_col_ = new_col[1:]
        # print("contr master: ", master.m.getConstrs())
        # print("new col len: ", len(new_col_))
        # myCol.addTerms(new_col, master.m.getConstrs())
        master1.m.update()
        iters1 += 1
        for i in master1.r.getVars():
            if i.varName == "Y0":
                y0 = i.x
        y0 = round(y0, 6)
        ceil_y0 = math.ceil(y0)
        floor_y0 = math.floor(y0)
        print("y0: ", y0)
        print("ceil: ", ceil_y0)
        print("floor: ", floor_y0)

    A2 = [[A[i][j] for j in range(len(A[0]))] for i in range(len(A))]
    S_M2 = [[S_M[i][j] for j in range(len(S_M[0]))] for i in range(len(S_M))]
    iters2 = 0
    master_sol2 = 0
    ceil_y0 = old_y0_ceil
    floor_y0 = floor_y0
    j = 0
    # finchè la y0 non è intera continuo a ciclare
    while (ceil_y0 != floor_y0):
        master2 = Master(A2, distance_matrix, K)
        master2.initModel(K)
        master2.extend_model_level1(ceil_y0, right=True)
        #
        # master2.m.computeIIS()
        # master2.m.write("Master2.ilp")
        try:
            master_sol2 = master2.resolve()
        except:
            break
        u = master2.getDualVariables()
        print("var duali: ", u)
        subproblem = Subproblem(distance_matrix, customers_x, Q, demands, time_windows, service_times, u)
        subproblem.m.update()
        # risolvo il sottoproblema
        res = subproblem.resolve()
        # Estraggo la nuova colonna da aggiungere ai vincoli
        new_col = subproblem.getNewCol()
        starting_times = subproblem.getStartingTimes()
        # print("----------> new col:", new_col)
        # print("constraint: ", master.getConstrs())

        print("------- Aggiungo al master la colonna: ------")
        print(new_col)

        A2 = addColumn(A2, new_col)
        S_M2 = addColumn(S_M2, starting_times)
        cost = calculate_route_capacity(createRoute(A2, len(A2[0]) - 1, S_M2), demands)
        print("Costo della nuova colonna in capacità: ", cost)
        print("---------------------")
        # tengo traccia dell'iterazione appena svolta

        # new_col_ = new_col[1:]
        # print("contr master: ", master.m.getConstrs())
        # print("new col len: ", len(new_col_))
        # myCol.addTerms(new_col, master.m.getConstrs())
        master2.m.update()

        iters2 += 1
        for i in master2.r.getVars():
            if i.varName == "Y0":
                y0 = i.x
        y0 = round(y0, 6)
        ceil_y0 = math.ceil(y0)
        floor_y0 = math.floor(y0)
        print("y0: ", y0)
        print("ceil: ", ceil_y0)
        print("floor: ", floor_y0)
else:
    print("Non è necessario effettuare il branching a due livelli, la soluzione è già intera")
# stampa dei risultati
print()
print()




print("========================== Master (before branching level 1) ================================")

print_model_variables(A, S_M, master, demands, Q)
routes = get_routes(master)
print()
print("valore FO  del Sottoproblema: ", res)
print("Valore FO master: ", master_sol)
print("Iterazioni tra master-column generator effettuate: ", iters)
print()

print("Before y0: ", y0_before)
print("After y0 : ", y0)
master1_flag = False
master2_flag = False
branchingLevel2 = False
if branching:

    routes = []
    history = []
    if master1.r.status == GRB.OPTIMAL:
        print("========================== Master 1 (after branching level 1) ================================")
        master1_flag = True
        print_model_variables(A1, S_M1, master1, demands, Q)

        routes = get_routes(master1)
        print()
        print("Valore FO master: ", master_sol1)
        print("Iterazioni tra master-column generator effettuate: ", iters1)
        print()


    if master2.r.status == GRB.OPTIMAL:
        master2_flag = True
        print("================== Master 2 (after branching level 1) ======================")
        print_model_variables(A2, S_M2, master2, demands, Q)
        routes = get_routes(master2)
        print()
        print("Valore FO master: ", master_sol2)
        print("Iterazioni tra master-column generator effettuate: ", iters2)
        print()





    if master1_flag:
        A = [[A1[i][j] for j in range(len(A1[0]))] for i in range(len(A1))]
        S_M = [[S_M1[i][j] for j in range(len(S_M1[0]))] for i in range(len(S_M1))]
        master = master1
    else:
        A = [[A2[i][j] for j in range(len(A2[0]))] for i in range(len(A2))]
        S_M = [[S_M2[i][j] for j in range(len(S_M2[0]))] for i in range(len(S_M2))]
        master = master2

    master_sol = master.resolve()
    #controllo se la soluzione è intera o frazionaria
    if check_fractional(master):
        branchingLevel2 = True
    else:
        branchingLevel2 = False

    if branchingLevel2:
        print("============================  Branching level 2 ======================")
        # vado ad inserire il vincolo che anche gli archi che uso siano interi
        starting_times = []

        # sono per il debugging
        p = 0
        master = Master(A, distance_matrix, K)
        master.initModel(K)
        master_sol = master.resolve()
        u = master.getDualVariables()
        subproblem1 = Subproblem(distance_matrix, customers_x, Q, demands, time_windows, service_times, u)
        subproblem2 = Subproblem(distance_matrix, customers_x, Q, demands, time_windows, service_times, u)
        u1 = u
        u2 = u
        k = 0
        master1_flag = False
        master2_flag = False

        m = 0
        while branchingLevel2:

            var = master.r.getVars()[m]
            routes = get_routes(master)
            print("varname:  {}, value: {}".format(var.varName, var.x))
            if var.varName != "Y0" and var.varName != "Yc":
                if 0 < var.x < 1:
                    print("=========== Modifico variabile: ", var.varName, "===========")
                    arcs = get_arcs(routes[k])
                    print("arcs: ", arcs)
                    for i, j in arcs:
                        # matrice secondaria che contiene il risultato del primo master
                        A1 = [[A[i][j] for j in range(len(A[0]))] for i in range(len(A))]
                        S_M1 = [[S_M[i][j] for j in range(len(S_M[0]))] for i in range(len(S_M))]

                        A1, S_M1, subproblem1 = modify_graph(subproblem1, A1, S_M1, 0, i, j, customers_x)

                        try:
                            res1 = subproblem1.resolve()
                            new_col = subproblem1.getNewCol()
                            starting_times = subproblem1.getStartingTimes()
                            print("Aggiungo colonnna sub1: ", new_col)
                            A1 = addColumn(A1, new_col)
                            S_M1 = addColumn(S_M1, starting_times)
                            cost = calculate_route_capacity(createRoute(A1, len(A1[0]) - 1, S_M1), demands)
                            print("Costo della nuova colonna in capacità: ", cost)
                            master1 = Master(A1, distance_matrix, K)
                            master1.initModel(K)
                            master_sol1 = master1.resolve()
                            #print_model_variables(A1, S_M1, master1, demands, Q)
                            u1 = master1.getDualVariables()
                            subproblem1.setU(u1)
                            # se ho la soluzione intera esco
                            if not check_fractional(master1):
                                branchingLevel2 = False
                                routes = get_routes(master1)
                                A = [[A1[i][j] for j in range(len(A1[0]))] for i in range(len(A1))]
                                S_M = [[S_M1[i][j] for j in range(len(S_M1[0]))] for i in range(len(S_M1))]

                                master1_flag = True
                                master2_flag = False
                                break

                        except:
                            master_sol1 = 0
                            # torna allo step precedente
                            A1 = [[A[i][j] for j in range(len(A[0]))] for i in range(len(A))]
                            S_M1 = [[S_M[i][j] for j in range(len(S_M[0]))] for i in range(len(S_M))]

                        # matrice secondaria che contiene il risultato del secondo master
                        A2 = [[A[i][j] for j in range(len(A[0]))] for i in range(len(A))]
                        S_M2 = [[S_M[i][j] for j in range(len(S_M[0]))] for i in range(len(S_M))]
                        A2, S_M2, subproblem2 = modify_graph(subproblem2, A2, S_M2, 1, i, j, customers_x)

                        try:
                            res2 = subproblem2.resolve()
                            new_col = subproblem2.getNewCol()
                            starting_times = subproblem2.getStartingTimes()
                            A2 = addColumn(A2, new_col)
                            print("Aggiungo colonnna sub2: ", new_col)
                            S_M2 = addColumn(S_M2, starting_times)
                            cost = calculate_route_capacity(createRoute(A2, len(A2[0]) - 1, S_M2), demands)
                            print("Costo della nuova colonna in capacità: ", cost)
                            master2 = Master(A2, distance_matrix, K)
                            master2.initModel(K)
                            master_sol2 = master2.resolve()
                            #print_model_variables(A2, S_M2, master2, demands, Q)
                            u2 = master2.getDualVariables()
                            subproblem2.setU(u2)
                            # se ho la soluzione intera esco
                            if not check_fractional(master2):
                                branchingLevel2 = False
                                routes = get_routes(master1)
                                A = [[A2[i][j] for j in range(len(A2[0]))] for i in range(len(A2))]
                                S_M = [[S_M2[i][j] for j in range(len(S_M2[0]))] for i in range(len(S_M2))]

                                master1_flag = False
                                master2_flag = True
                                break
                        except:
                            master_sol2 = 0
                            # torna allo step precedente
                            A2 = [[A[i][j] for j in range(len(A[0]))] for i in range(len(A))]
                            S_M2 = [[S_M[i][j] for j in range(len(S_M[0]))] for i in range(len(S_M))]
                        print("master sol1: ", master_sol1)
                        print("master sol2 : ", master_sol2)
                        if master_sol1 == 0 and master_sol2 == 0:
                            continue
                        if master_sol1 <= master_sol2 and master_sol1 != 0:
                            print("Aggiorno la matrice con sub1")
                            #print_model_variables(A1, S_M1, master1, demands, Q)
                            A = [[A1[i][j] for j in range(len(A1[0]))] for i in range(len(A1))]
                            S_M = [[S_M1[i][j] for j in range(len(S_M1[0]))] for i in range(len(S_M1))]
                            cost = calculate_route_capacity(createRoute(A, len(A[0]) - 1, S_M), demands)
                            history.append(("sub1", cost))

                        elif master_sol2 < master_sol1 and master_sol2 != 0:
                            print("Aggiorno la matrice con sub2")
                            #print_model_variables(A2, S_M2, master2, demands, Q)
                            A = [[A2[i][j] for j in range(len(A2[0]))] for i in range(len(A2))]
                            S_M = [[S_M2[i][j] for j in range(len(S_M2[0]))] for i in range(len(S_M2))]
                            cost = calculate_route_capacity(createRoute(A, len(A[0]) - 1, S_M), demands)
                            history.append(("sub2", cost))

                        elif master_sol1 == 0 and master_sol2 != 0:
                            print("Aggiorno la matrice con sub2")
                            #print_model_variables(A2, S_M2, master2, demands, Q)
                            A = [[A2[i][j] for j in range(len(A2[0]))] for i in range(len(A2))]
                            S_M = [[S_M2[i][j] for j in range(len(S_M2[0]))] for i in range(len(S_M2))]
                            cost = calculate_route_capacity(createRoute(A, len(A[0]) - 1, S_M), demands)
                            history.append(("sub2_2", cost))

                        elif master_sol1 != 0 and master_sol2 == 0:
                            print("Aggiorno la matrice con sub1")
                            #print_model_variables(A1, S_M1, master1, demands, Q)
                            A = [[A1[i][j] for j in range(len(A1[0]))] for i in range(len(A1))]
                            S_M = [[S_M1[i][j] for j in range(len(S_M1[0]))] for i in range(len(S_M1))]
                            cost = calculate_route_capacity(createRoute(A, len(A[0]) - 1, S_M), demands)
                            history.append(("sub1_2", cost))

                        k += 1
                    if branching:
                        master = Master(A, distance_matrix, K)
                        master.initModel(K)
                        master_sol = master.resolve()

                    #print_model_variables(A, S_M, master, demands, Q)

                    #sys.exit(0)
            m += 1

        print(" =============== After branching level 2 ====================")

        master = Master(A, distance_matrix, K)
        master.initModel(K)
        master_sol = master.resolve()
        print_model_variables(A, S_M, master, demands, Q)
        routes = get_routes(master)
        print("Valore FO: ", master_sol)

print("=========================== Branching level 3 ======================")
# se i costi non sono ancora interi effettuo il branching livello 3

if master1_flag:
    yc = master_sol1
elif master2_flag:
    yc = master_sol2
ceil_yc = math.ceil(yc)
floor_yc = math.floor(yc)
old_yc_ceil = ceil_yc
old_yc_floor = floor_yc
print("original value: ", yc)
print("ceil: ", ceil_yc)
print("floor: ", floor_yc)

iters = 0
master_sol2 = 0
ceil_yc = old_yc_ceil
floor_yc = old_yc_floor
j = 0
# finchè la y0 non è intera continuo a ciclare
branchingLevel3 = False
if ceil_yc != floor_yc:
    branchingLevel3 = True
else:
    print("Branching level 3 non necessario")

while (ceil_yc != floor_yc):
    master = Master(A, distance_matrix, K)
    master.initModel(K)
    master.extend_model_level3(ceil_yc, right=True)

    try:
        master_sol = master.resolve()
    except:
        break
    u = master.getDualVariables()
    print("var duali: ", u)
    subproblem = Subproblem(distance_matrix, customers_x, Q, demands, time_windows, service_times, u)
    subproblem.m.update()
    # risolvo il sottoproblema
    res = subproblem.resolve()
    # Estraggo la nuova colonna da aggiungere ai vincoli
    new_col = subproblem.getNewCol()
    starting_times = subproblem.getStartingTimes()
    # print("----------> new col:", new_col)
    # print("constraint: ", master.getConstrs())

    print("------- Aggiungo al master la colonna: ------")
    print(new_col)
    print("---------------------")
    A = addColumn(A, new_col)
    S_M = addColumn(S_M, starting_times)
    # tengo traccia dell'iterazione appena svolta

    # new_col_ = new_col[1:]
    # print("contr master: ", master.m.getConstrs())
    # print("new col len: ", len(new_col_))
    # myCol.addTerms(new_col, master.m.getConstrs())
    master.m.update()

    iters += 1
    for i in master.r.getVars():
        if i.varName == "Yc":
            yc = i.x
    yc = round(yc, 6)
    ceil_yc = math.ceil(yc)
    floor_yc = math.floor(yc)
    print("yc: ", yc)
    print("ceil: ", ceil_yc)
    print("floor: ", floor_yc)

if branchingLevel3:
    if master.r.status == GRB.OPTIMAL:

        print("================== Master  (after branching level 3) ======================")
        print_model_variables(A, S_M, master, demands, Q)

        print()
        print("Valore FO master: ", master_sol)
        print("Iterazioni tra master-column generator effettuate: ", iters)
        print()

        routes = get_routes(master)
# controllo sulle rotte
if not check_routes(A, routes, customers_x):
    print("Le rotte create sono errate!")
    sys.exit(1)

# print_matrix(A)



print()
print("========================== COMPACT MODEL ================================")

print("FO modello compatto: ", resComp)
