from gurobipy import *


class Master:
    def __init__(self, matrix, distance_matrix, K):
        self.m = Model("VRPT_master")
        #self.m.Params.LogToConsole = 0
        self.A = matrix
        self.distance_matrix = distance_matrix
        self.c = dict()
        # compute initial costs
        # si parte dal deposito

        print("Righe: {}".format(len(self.A)))
        print("Colonne: {}".format(len(self.A[0])))
        last_client_served = 0
        for j in range(len(self.A[0])):
            actual_client = 0
            tot = 0
            for i in range(len(self.A)):
                tot += self.A[i][j] * self.distance_matrix[actual_client][i]
                if self.A[i][j] != 0:
                    # print(str(self.distance_matrix[actual_client][i])+" + ", end='')
                    last_client_served = i
                actual_client = i
            tot += self.distance_matrix[last_client_served][0]
            # print(self.distance_matrix[last_client_served][0], end='')
            self.c[j] = tot
            # print()
        print("Costi percorsi: ")
        print(self.c)

    def setMatrix(self, matrix):
        self.A = matrix
        last_client_served = 0
        for j in range(len(self.A[0])):
            actual_client = 0
            tot = 0
            for i in range(len(self.A)):
                tot += self.A[i][j] * self.distance_matrix[actual_client][i]
                if self.A[i][j] != 0:
                    # print(str(self.distance_matrix[actual_client][i])+" + ", end='')
                    last_client_served = i
                actual_client = i
            tot += self.distance_matrix[last_client_served][0]
            # print(self.distance_matrix[last_client_served][0], end='')
            self.c[j] = tot
            # print()
        print("Costi percorsi: ")
        print(self.c)


    # metodo che va ad inizializzare il modello
    def initModel(self, K):
        self.C = range(1, len(self.A))
        self.P = range(1,len(self.A[0]))
        self.y = self.m.addVars(self.P, name="y", vtype=GRB.BINARY)
        self.y0 = self.m.addVar(name="Y0", vtype=GRB.INTEGER)
        self.yc = self.m.addVar(name="Yc", vtype=GRB.INTEGER)
        # numero veicoli
        self.K = K

        # print("P", P)
        # print("righe: ", len(self.A))
        # print("colonne: ", len(self.A[0]))

        self.m.addConstrs((quicksum(self.A[i][p] * self.y[p] for p in self.P) == 1 for i in self.C),
                          name="one_client_one_route")
        #self.m.addConstr((quicksum(self.y[p] for p in self.P) <= self.K), name="max_vehicles_number")
        self.m.addConstr(((quicksum(self.y[p] for p in self.P) - self.y0) == 0), name="equivalent_1")
        self.m.addConstr(((quicksum(self.c[p] * self.y[p] for p in self.P) - self.yc) == 0), name="equivalent_2")
        #self.m.addConstr(self.y[0] == 0)
        self.m.setObjective(quicksum(self.c[p] * self.y[p] for p in self.P), GRB.MINIMIZE)


    def __set_num_vehicle_used(self, value):
        self.Y0 = value

    def __set_total_cost(self, value):
        self.Yc = value

    def extend_model_level1(self, y0, left=False, right=False):
        if left:
            self.m.addConstr((quicksum(self.y[p] for p in self.P) <= y0), name="Integrality_y0_left")
        elif right:
            self.m.addConstr((quicksum(self.y[p] for p in self.P) >= y0), name="integrality_y0_right")

    def extend_model_level3(self, yc, left=False, right=False):
        if left:
            self.m.addConstr((quicksum(self.c[p] * self.y[p] for p in self.P) <= yc), name="Integrality_yc_left")
        elif right:
            self.m.addConstr((quicksum(self.c[p] * self.y[p] for p in self.P) >= yc), name="integrality_yc_right")

    def resolve(self):
        self.m.update()
        self.r = self.m.relax()
        # self.m.optimize()
        self.r.optimize()
        res = self.r.getObjective().getValue()
        # print("Res: ", res)
        return res

    def getDualVariables(self):
        cnt = []
        for c in self.r.getConstrs():
            cnt.append(c.pi)
        C = range(1, len(self.A))
        x = [i for i in C]
        u = dict(zip(x, cnt))
        # print(u)
        return u
