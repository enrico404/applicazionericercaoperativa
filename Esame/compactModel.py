from gurobipy import *


class CompactModel:
    def __init__(self, distance_matrix, K, Q, demands, time_windows, service_times, customers_x):
        self.m = Model("VRPT_compact")
        self.A = [[0 for j in range(len(customers_x))] for i in range(len(customers_x))]
        self.distance_matrix = distance_matrix
        self.Q = Q
        self.demands = demands
        self.time_windows = time_windows
        self.service_times = service_times
        self.K = K

    def resolve(self):
        C = range(1, len(self.A))
        X = {(i, j, k): 0 for k in range(self.K) for j in range(len(self.A[0])) for i in range(len(self.A))}
        x = self.m.addVars(X.keys(), name="x", vtype=GRB.BINARY)

        S = {(i, k): self.time_windows[i][0] for k in range(self.K) for i in range(len(self.time_windows))}
        s = self.m.addVars(S.keys(), name="s", vtype=GRB.CONTINUOUS)

        # self.m.addConstrs((quicksum(x[i, j, k] for j in range(len(self.A))) for k in range(self.K)) == 1
        #                   for i in range(len(self.A)))

        self.m.addConstrs((quicksum(
            quicksum(x[i, j, k] for j in C) for k in range(self.K)) == 1
                           for i in C), name="serve_all_customers")

        self.m.addConstrs((quicksum(self.demands[i] * quicksum(x[i, j, k] for j in C)
                                    for i in C)
                           <= self.Q for k in range(self.K)), name="vehicle_capacity")

        # esco dal deposito una sola volta per veicolo
        self.m.addConstrs((quicksum(x[0, j, k] for j in C) == 1 for k in range(self.K)),
                          name="routing_costraint_1")

        # vincolo di bilanciamento del flusso, se entro sono costretto ad uscire
        self.m.addConstrs(
            (quicksum(x[i, h, k] for i in C) - quicksum(x[h, j, k] for j in C) == 0
             for h in C for k in range(self.K)), name="routing_costraint_2")

        # entro nel deposito una sola volta per veicolo
        self.m.addConstrs((quicksum(x[i, 0, k] for i in C) == 1 for k in range(self.K)),
                          name="routing_costraint_3")

        # # introduco il concetto di tempo
        # self.m.addConstrs((s[i, k] + self.distance_matrix[i][j] + self.service_times[i] - max(self.time_windows[i][1] +
        #                                                                                       self.distance_matrix[i][j]
        #                                                                                       + self.service_times[i]
        #                                                                                       - self.time_windows[i][0]
        #                                                                                       for i in
        #                                                                                       range(len(self.A))
        #                                                                                       for j in
        #                                                                                       range(len(self.A[0]))
        #                                                                                       )) <= s[j, k]
        #                   for i in range(len(self.A)) for j in range(len(self.A[0])) for k in range(self.K)
        #                   )

        self.m.addConstrs((s[i, k] + self.distance_matrix[i][j] - max(self.time_windows[i][1] +
                                                                      self.distance_matrix[i][j]
                                                                      - self.time_windows[i][0]
                                                                      for i in
                                                                      range(len(self.A))
                                                                      for j in
                                                                      range(len(self.A[0]))
                                                                      ) * (1 - x[i, j, k])) <= s[j, k]
                          for i in range(len(self.A)) for j in range(len(self.A[0])) for k in range(self.K)
                          )

        self.m.addConstrs(s[i, k] <= self.time_windows[i][1] for i in C for k in range(self.K))
        self.m.addConstrs(self.time_windows[i][0] <= s[i, k] for i in C for k in range(self.K))

        self.m.setObjective(quicksum(quicksum(self.distance_matrix[i][j] * x[i, j, k] for i in range(len(self.A))
                                              for j in range(len(self.A[0])))
                                     for k in range(self.K)), GRB.MINIMIZE)

        self.m.update()
        self.m.write("VRPT_compact.lp")
        # self.m.computeIIS()
        # self.m.write("VRTP_compact.ilp")
        self.m.optimize()
        res = self.m.getObjective().getValue()
        print("Res: ", res)

        # for i in master.m.getVars():
        #     print(i.varName, ":", i.x)
        return res
