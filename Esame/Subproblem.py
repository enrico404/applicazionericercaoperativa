from gurobipy import *
import re

class Subproblem:
    def __init__(self, distance_matrix, customers, Q, demands, time_windows, service_times, u):
        self.u = {}
        self.m = Model("VRPT_subproblem")
        #self.m.Params.LogToConsole = 0
        self.distance_matrix = distance_matrix
        self.Q = Q
        self.u = u
        self.demands = demands
        self.time_windows = time_windows
        self.service_times = service_times
        self.customers = customers
        self.init_model()

    def init_model(self):
        self.newCol = [0 for i in range(len(self.customers))]
        self.starting_times = [0 for i in range(len(self.customers))]
        X = {(i, j): 0 for i in range(len(self.customers)) for j in range(len(self.customers))}
        self.x = self.m.addVars(X.keys(), name="x", vtype=GRB.BINARY)
        S = {i: self.time_windows[i][0] for i in range(len(self.time_windows))}
        self.s = self.m.addVars(S.keys(), name="s", vtype=GRB.CONTINUOUS)

        C = range(1, len(self.customers))

        # x = self.m.addVars(range(len(self.A)), name="x", vtype=GRB.BINARY)

        self.m.addConstr((quicksum(
            self.demands[i] * quicksum(self.x[i, j] for j in range(0, len(self.customers))) for i in C) <= self.Q),
                         name="capacity_constraint")

        self.m.addConstr((quicksum(self.x[0, j] for j in range(0, len(self.customers))) == 1),
                         name="routing_constraint_1")

        # self.m.addConstrs(
        #     (quicksum(x[i, h] - x[h, j] for i in C for j in C) == 0
        #     for h in C), name="routing_constraint_2")

        self.m.addConstrs(
            (quicksum(self.x[i, h] for i in range(0, len(self.customers))) - quicksum(
                self.x[h, j] for j in range(0, len(self.customers))) == 0 for h in C),
            name="routing_constraint_2")

        self.m.addConstr(quicksum(self.x[i, 0] for i in range(0, len(self.customers))) == 1)

        # HO AGGIUNTO ANCHE IL TEMPO DI SERVIZIO PERCHè LA CONSEGNA NON è ISTANTANEA
        # se percorro l'arco ij il tempo di partenza + il tempo di travelling + il tempo di servizio, deve essere minore
        # o uguale del tempo di partenza del nodo j
        # self.m.addConstrs((s[i] + self.distance_matrix[i][j] + self.service_times[i] - max(self.time_windows[i][1] +
        #                                                                                    self.distance_matrix[i][j]
        #                                                                                    + self.service_times[i]
        #                                                                                    - self.time_windows[i][0]
        #                                                                                    for i in
        #                                                                                    range(len(self.customers))
        #                                                                                    for j in
        #                                                                                    range(len(self.customers))
        #                                                                                    )) <= s[j]
        #                   for i in range(len(self.customers)) for j in range(len(self.customers))
        #                   )
        # suppongo che il tempo di servizio sia istantaneo
        self.m.addConstrs(
            (self.s[i] + self.distance_matrix[i][j] - max(self.time_windows[i][1]
                                                          + self.distance_matrix[i][j]
                                                          - self.time_windows[i][0]
                                                          for i in range(len(self.customers))
                                                          for j in range(len(self.customers)))
             * (1 - self.x[i, j])) <= self.s[j]
            for i in range(len(self.customers)) for j in range(len(self.customers))
        )
        # non esiste 1<x<2, allora lo spezzo in due
        # self.m.addConstrs(self.time_windows[i][0] <= s[i] <= self.time_windows[i][1] for i in range(len(self.A)))
        self.m.addConstrs(self.s[i] <= self.time_windows[i][1] for i in C)
        self.m.addConstrs(self.time_windows[i][0] <= self.s[i] for i in C)

        # print("righe: ", len(self.distance_matrix))
        # print("colonne: ", len(self.distance_matrix[0]))

        self.m.setObjective(quicksum(
            (self.distance_matrix[i][j] - self.u[i]) * self.x[i, j] for i in C for j in C), GRB.MINIMIZE)

    def resolve(self):

        self.newCol = [0 for i in range(len(self.customers))]
        self.starting_times = [0 for i in range(len(self.customers))]

        self.m.update()
        self.m.optimize()

        for i in self.m.getVars():
            if str(i.varName).startswith("x["):
                if i.x == 1:

                    # estraggo i clienti visitati dall'indice della variabile e quelli visitati li metto a 1
                    tmp = re.findall(r'\d+', i.varName)[0]
                    # print(i.varName, ": ", i.x)
                    # print("tmp: ", tmp)
                    self.newCol[int(tmp)] = 1
            elif str(i.varName).startswith("s["):
                tmp = re.findall(r'\d+', i.varName)[0]
                self.starting_times[int(tmp)] = i.x
            # else:
            #     print(i.varName, ": ", i.x)

        # print("nuova colonna:")
        # print(self.newCol)
        res = self.m.getObjective().getValue()
        # print("Res: ", res)
        return res

    # metodo che ritorna la nuova colonna da aggiungere al problema master
    def getNewCol(self):
        return self.newCol

    # metodo che setta il valore delle variabili duali calcolate dal master
    def setU(self, u):
        self.u = u

    def getGraph(self):
        return self.x

    def getStartingTimes(self):
        return self.starting_times
