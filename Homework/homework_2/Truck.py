import networkx as nx


class Truck:
    '''
    classe Truck, che si occupa della gestione del singolo camion
    '''
    def __init__(self, capacity):
        '''
        metodo di init del camion
        :param capacity: capacità del camion
        '''
        self.route = [0]
        self.capacity = capacity
        self.distance_traveled = 0
        self.neighborhood = []

    def get_capacity(self):
        '''
        getter della capacitò
        :return: capacità del camion
        '''
        return self.capacity

    def get_route(self):
        '''
        getter della route corrente
        :return: rotta corrente del camion
        '''
        return self.route

    def set_capacity(self, c):
        '''
        setter della capacità del camiom
        :param c: capacità
        :return:
        '''
        self.capacity = c

    def get_distance_traveled(self):
        '''
        getter della distance_traveled
        :return: distanza percorsa
        '''
        return self.distance_traveled

    def add_client(self, qi, client_id, travel_distance):
        '''
        Metodo che aggiunge un cliente alla route corrente del camion
        :param qi: richiesta del cliente
        :param client_id: id del cliente
        :param travel_distance: distanza per raggiungere il cliente
        :return:
        '''
        if ((self.capacity - qi) >= 0):
            #print("Capacità truck: {}, tolgo: {} ".format(self.capacity, qi))
            self.capacity -= qi
        else:
            print("The truck does not have sufficient capacity remaining!")
            return 1
        self.route.append(client_id)
        self.distance_traveled += travel_distance

    def ret_to_depot(self, travel_distance):
        '''
        Metodo che chiude il ciclo del camion, aggiunge il deposito come rotta finale
        :param travel_distance: distanza al deposito
        :return:
        '''
        self.route.append(0)
        self.distance_traveled += travel_distance

    def add_alternative_route(self, list):
        '''
        Metodo che aggiunge una route alla matrice che memorizza il vicinato
        :param list: nuova route da aggiungere
        :return:
        '''
        for el in list:
            for i in range(len(self.route)):
                for j in range(len(self.route)):
                    self.neighborhood[i][j] = el

