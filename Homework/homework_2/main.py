import math
import sys
import networkx as nx
from Truck import Truck
import matplotlib.pyplot as plt


def compute_distance_matrix(n_customers, customers_x, customers_y, depot_x, depot_y):
    #funzione che calcola la matrice delle distanze
    distance_matrix = [[None for i in range(n_customers + 1)] for j in range(n_customers + 1)]
    all_x = []
    all_y = []
    all_x.append(depot_x)
    all_x += customers_x
    all_y.append(depot_y)
    all_y += customers_y

    for i in range(n_customers + 1):
        distance_matrix[i][i] = 0
        for j in range(n_customers + 1):
            dist = compute_eucledian_distance(all_x[i], all_x[j], all_y[i], all_y[j])
            distance_matrix[i][j] = dist
            distance_matrix[j][i] = dist
    return distance_matrix


def compute_eucledian_distance(xi, xj, yi, yj):
    #funzione alla quale dati due punti x e y, calcola la distanza euclidea tra i questi due
    exact_dist = math.sqrt(math.pow(xi - xj, 2) + math.pow(yi - yj, 2))
    return int(math.floor(exact_dist + 0.5))


def compute_distance_warehouses(depot_x, depot_y, customers_x, customers_y):
    #funzione che calcola la distanza dal cliente i-esimo al deposito.
    #NB: funzione deprecata
    distance_warehouses = []
    for i in range(len(customers_x)):
        dist = compute_eucledian_distance(depot_x, customers_x[i], depot_y, customers_y[i])
        distance_warehouses.append(dist)
    return distance_warehouses


def find_min(list, visited_nodes):
    #funzione che cerca il nodo più piccolo all'interno di una lista, escludendo gli indici specificati nella lista
    #chiamata "visited_nodes"
    i = 0
    min_ = list[i]
    index_min = i
    while i < len(list):
        if min_ > list[i] and i not in visited_nodes:
            min_ = list[i]
            index_min = i
        i += 1
    return min_, index_min


def find_nearest_neighbour(distance_matrix, index, customers_x, visited_nodes, not_feasable_nodes):
    # funzione che dato un nodo con indice "index", va a cercare nella matrice delle distanze il primo vicino
    # ammissibile più vicino
    min_ = 9999
    node_id = 9999
    for j in range(len(customers_x) + 1):
        if min_ > distance_matrix[index][j] and j not in visited_nodes and j not in not_feasable_nodes:
            # se ho indici uguali, vuol dire che avrò certamente zero come ditanza, perchè è il cliente stesso
            # devo anche non passare per il magazzino come nodo intermedio
            if index == j or j == 0:
                continue
            min_ = distance_matrix[index][j]
            node_id = j
    # print("nodo_min: {}".format(node_id))
    return node_id, min_


def print_truck_routes(trucks):
    i = 0
    for t in trucks:
        print("Truck {}: {}".format(i, t.route))
        print("Capacity remaining: {}".format(t.capacity))
        print("Distance traveled: {} ".format(t.distance_traveled))
        print()
        i += 1


def compute_greedy_routes(customers_x, distance_matrix, capacity, demands, trucks, visited_nodes):
    # funzione per il calcolo delle rotte dei trucks in maniera greedy costruttiva
    m = len(trucks)
    # devo verificare che la somma totale delle capacità dei camion sia maggiore o uguale alla somma delle quantità
    #richieste dai clienti
    if (m * capacity < sum(demands)):
        print("Non hai abbastanza camion per gestire tutti i clienti!")
        sys.exit(1)

    # m rotte
    for i in range(m):
        first_client, travel_distance = find_nearest_neighbour(distance_matrix, 0, customers_x, visited_nodes,
                                                               not_feasable_nodes=[])
        # print("starting node: ", first_client, " with qi: ", demands[first_client])

        qi = demands[first_client]
        trucks[i].add_client(qi, first_client, travel_distance)
        # print("Truck[{}] aggiungo tappa (cliente: {}) alla distanza: {}".format(i, first_client,travel_distance))
        visited_nodes.append(first_client)
        not_feasable_nodes = []
        while True:
            node_id, travel_distance = find_nearest_neighbour(distance_matrix,
                                                              trucks[i].route[len(trucks[i].route) - 1], customers_x,
                                                              visited_nodes, not_feasable_nodes)
            if node_id == 9999:
                # non trovo più nodi da visitare ammissibili, faccio tornare il furgone al magazzino e chiudo il ciclo

                km_to_depot = distance_matrix[0][trucks[i].route[len(trucks[i].route) - 1]]
                trucks[i].ret_to_depot(km_to_depot)
                # print("Truck[{}] aggiungo tappa (cliente: {}) alla distanza: {}".format(i, 0, km_to_depot))
                break
            # print("pesco quantità richiesta per nodo: ", node_id)
            qi = demands[node_id]
            if (trucks[i].capacity - qi >= 0):
                trucks[i].add_client(qi, node_id, travel_distance)
                # print("Truck[{}] aggiungo tappa (cliente: {}) alla distanza: {}".format(i,node_id, travel_distance))
                visited_nodes.append(node_id)
            else:
                not_feasable_nodes.append(node_id)
    print_truck_routes(trucks)


def compute_total_distance_traveled(trucks):
    #funzione per il calcolo della distanza totale percorsa dai trucks
    tot = 0
    for t in trucks:
        tot += t.distance_traveled
    return tot


def check_routes(trucks, customers):
    # check the correctness of the routes
    list = []
    for t in trucks:
        for el in t.route:
            if el != 0:
                list.append(el)
    list.sort()
    check_list = [i for i in range(1, len(customers)+1)]
    if list == check_list:
        return True
    else:
        print("lista per il debug:")
        print(list)
        print("come dovrebbe essere:")
        print(check_list)
        return False


def swap_two_elements(list, i, j):
    #funzione che data una lista e due indici, swappa i due elementi che si trovano nelle posizioni i e j
    a = list[i]
    b = list[j]
    tmp = a
    a = b
    b = tmp

    list[i] = a
    list[j] = b
    return list


def neighborhood(trucks):
    #funzione per il calcolo dei vicinati delle rotte dei vari trucks
    i = 0
    while i < len(trucks):
        original = trucks[i].route
        for j in range(1, len(trucks[i].route)):
            for k in range(j + 1, len(trucks[i].route) - 1):
                list = original.copy()
                list = swap_two_elements(list, j, k)
                # print("Original:")
                # print(trucks[i].route)
                # print("New:")
                # print(list)
                trucks[i].neighborhood.append(list)
        i += 1


def compute_distance_route(route, distance_matrix):
    #funzione che calcola la distanza totale in una ruote
    tot_distance = 0
    i = 0
    j = 1
    while (j < len(route)):
        index_a = route[i]
        index_b = route[j]
        # print("Sommo distanza: {} -> {} : {}".format(index_a, index_b, distance_matrix[index_a][index_b]))
        tot_distance += distance_matrix[index_a][index_b]
        i += 1
        j += 1
    return tot_distance


def compute_min_neighbour(trucks, distance_matrix):
    #funzione che va a cercare la rotta meno costosa (in termini di distanza) all'interno del vicinato disponibile e la
    #assegna al truck i-esimo. La funzione ritorna poi la nuova distanza totale dei vari percorsi.
    for i in range(len(trucks)):
        min_ = compute_distance_route(trucks[i].route, distance_matrix)
        # print("Distanza truck[{}] calcolata: {}, distanza giusta: {}".format(i, min_, trucks[i].distance_traveled))
        for j in range(len(trucks[i].neighborhood)):
            new = compute_distance_route(trucks[i].neighborhood[j], distance_matrix)
            if min_ > new:
                min_ = new
                trucks[i].route = trucks[i].neighborhood[j]
                old = trucks[i].distance_traveled
                trucks[i].distance_traveled = min_
                print("Trovato un percorso migliore per il truck[{}]: {}".format(i, trucks[i].route))
                print("New travelling distance: {} (before) -> {}".format(old, min_))
    new_total_distance = compute_total_distance_traveled(trucks)
    return new_total_distance


def localSearch(customers_x, distance_matrix, capacity, demands, trucks):
    visited_nodes = []
    print("--------------------Greedy solution--------------------")
    compute_greedy_routes(customers_x, distance_matrix, capacity, demands, trucks, visited_nodes)
    distance_traveled = compute_total_distance_traveled(trucks)
    print("Total distance traveled by trucks: ", distance_traveled)

    continue_ = True
    x = distance_traveled
    print("-------------------Local Search-----------------------")
    # calcolo il neighbohood
    neighborhood(trucks)
    #algoritmo di local search vero e proprio
    while continue_:
        x_ = compute_min_neighbour(trucks, distance_matrix)
        if x_ < x:
            x = x_
        else:
            continue_ = False

    print("--------------------After Local Search-------------------------")
    print_truck_routes(trucks)
    distance_traveled = compute_total_distance_traveled(trucks)
    print("Total distance traveled by trucks: ", distance_traveled)


def plot_dataset(customers_x, customers_y, depot_x, depot_y):
    # funzione per il debugging, plotta i nodi in un grafico
    data_x = []
    data_y = []
    data_x.append(depot_x)
    data_x += customers_x
    data_y.append(depot_y)
    data_y += customers_y
    plt.scatter(data_x, data_y)

    labels = [i for i in range(len(customers_x) + 1)]
    for x, y, label in zip(data_x, data_y, labels):
        plt.annotate(label, xy=(x, y))
    plt.show()


def create_graph(trucks, customers_x, customers_y, depot_x, depot_y):
    #funzione che va a disegnare il grafo finale del problema risolto con la local search.
    G = nx.Graph()
    for i in range(len(customers_x) + 1):
        G.add_node(i)

    for t in trucks:
        i = 0
        j = 1
        while j < len(t.route):
            G.add_edge(t.route[i], t.route[j], len=distance_matrix)
            i += 1
            j += 1

    data_x = []
    data_y = []
    data_x.append(depot_x)
    data_x += customers_x
    data_y.append(depot_y)
    data_y += customers_y

    pos = {i: 0 for i in range(len(data_x))}
    i = 0
    for x, y in zip(data_x, data_y):
        pos[i] = [x, y]
        i+= 1


    nx.draw_networkx_nodes(G, pos=pos)
    nx.draw_networkx_labels(G, pos=pos)
    import numpy as np
    for t in trucks:
        color = (np.random.uniform(0, 1), np.random.uniform(0, 1), np.random.uniform(0, 1), 1)
        edgelist = []
        i = 0
        j = 1
        while j < len(t.route):
            tmp = (t.route[i], t.route[j])
            edgelist.append(tmp)
            i += 1
            j += 1
        nx.draw_networkx_edges(G, edgelist=edgelist, edge_color=color, pos=pos)

    #nx.draw(G, with_labels=True, pos=pos, edge_color=c)
    plt.show()


filename = "./data/E076-07s.dat"
#filename = "./data/E033-04g.dat"
#filename = "./data/E021-04m.dat"

with open(filename, "r") as f:
    data = [str(elem) for elem in f.read().split()]
    data_iter = iter(data)

n_customers = 0
#parsing del dataset
while (True):
    # estraggo un elemento
    el = next(data_iter)
    if el == "NAME":
        # rimuove :
        next(data_iter)
        # estraggo il nome del dataset
        dataset_name = next(data_iter)
        print("Nome del dataset: ", dataset_name)
    if el == "TYPE":
        # rimuove :
        next(data_iter)
        type_ = next(data_iter)
    if el == "DIMENSION":
        # rimuove :
        next(data_iter)
        dimension = int(next(data_iter))
        n_customers = dimension - 1
        print("Dimension: ", dimension)
    if el == "EDGE_WEIGHT_TYPE":
        # rimuove :
        next(data_iter)
        edge_weight_type = next(data_iter)
        print("Edge weight type: ", edge_weight_type)
    if el == "CAPACITY":
        # rimuove :
        next(data_iter)
        capacity = int(next(data_iter))
        print("Capacity: ", capacity)
    if el == "VEHICLES":
        # rimuove :
        next(data_iter)
        vehicles = int(next(data_iter))

    if el == "NODE_COORD_SECTION":
        depot_x = 0
        depot_y = 0
        customers_x = []
        customers_y = []
        for i in range(dimension):
            node_id = int(next(data_iter))
            if node_id == 1:
                depot_x = int(float(next(data_iter)))
                depot_y = int(float(next(data_iter)))
            else:
                x = int(float(next(data_iter)))
                y = int(float(next(data_iter)))
                #print("x: {}, y: {}".format(x, y))
                customers_x.append(x)
                customers_y.append(y)

        distance_matrix = compute_distance_matrix(n_customers, customers_x, customers_y, depot_x, depot_y)
        #distance_warehouses = compute_distance_warehouses(depot_x, depot_y, customers_x, customers_y)
        #print("-----Distance matrix-----")
        #print(distance_matrix)
        # print("-----Distance warehouses-----")
        # print(distance_warehouses)
        #print("Dep_x: {}, depy: {}".format(depot_x, depot_y))

    if el == "DEMAND_SECTION":
        demands = []

        for i in range(dimension):
            node_id = int(next(data_iter))
            # print("node id: ", node_id)
            if node_id == 1:
                if int(next(data_iter)) != 0:
                    print("Demand for depot must be 0")
                    sys.exit(1)
                # la richiesta del deposito è sempre 0
                demands.append(0)
            else:
                el = int(next(data_iter))
                demands.append(el)
        #print("Demands:")
        #print(demands)
        break

trucks = []
for t in range(vehicles):
    trucks.append(Truck(capacity))

#algoritmo di Local Search
localSearch(customers_x, distance_matrix, capacity, demands, trucks)

#vado a controllare che le rotte impostate per i trucks siano corrette e non manchino dei clienti da servire
if not check_routes(trucks, customers_x):
    print("The routes assigned to the trucks are wrong!")
    sys.exit(1)


#vado a plottare il risultato della Local Search
create_graph(trucks, customers_x, customers_y, depot_x, depot_y)

# tutto quello che c'è sotto questa funzione non va, tienilo in conto
# plot_dataset(customers_x, customers_y, depot_x, depot_y)
