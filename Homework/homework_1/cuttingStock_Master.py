from gurobipy import *


class CuttingStock_master:
    '''
    Classe che implementa il problema master
    '''
    def __init__(self, items, l, n, L, matrix):
        self.items = items
        self.l = l
        self.n = n
        self.L = L
        self.m = Model("CS_master")
        self.matrix = matrix

    #metodo getter che ritorna la matrice delle colonne su cui viene calcolato il modello
    def getMatrix(self):
        return self.matrix

    #setter per la matrice delle colonne
    def setMatrix(self, matrix):
        self.matrix = matrix

    #metodo che va ad inizializzare il modello
    def initModel(self):
        items_key = range(len(self.items))
        J = range(len(self.matrix))
        # print(self.matrix)
        # print("len:", len(self.matrix))

        y = self.m.addVars(J, name="y", vtype=GRB.INTEGER)

        self.m.addConstrs((quicksum(y[j] * self.matrix[j][i] for j in J) >= self.n[i] for i in items_key))

        self.m.setObjective(y.sum(), GRB.MINIMIZE)


    #metodo che va ad aggiornare il modello, lo va a rilassare e poi ottimizzare
    def resolve(self):
        #self.m.optimize()
        self.m.update()
        self.r = self.m.relax()
        self.r.optimize()

        solRelaxed = self.r.getObjective().getValue()
       # sollInt = self.m.getObjective().getValue()
        return solRelaxed

    #metodo che ritorna le variabili duali calcolate dal modello e che verranno poi passate al sottoproblema
    # in formato di dizionario
    def getDualVariables(self):
        cnt = []
        for c in self.r.getConstrs():
            #print(c, c.pi)
            cnt.append(c.pi)

        u = dict(zip(self.items, cnt))
       # print(u)
        return u

