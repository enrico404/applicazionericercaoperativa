from cuttingStock_Master import CuttingStock_master
from cuttingStock_Slave import CuttingStock_slave
from gurobipy import *

if __name__== "__main__":
    # ----------- DATI -------------

    # nomi degli items
    items = ['a', 'b', 'c', 'd']

    # lunghezza degli items richiesta
    l = {'a': 50, 'b': 73, 'c': 92, 'd': 121}

    # quantità degli items richieste
    n = [150, 200, 300, 200]

    # lunghezza dei rolls
    L = 1800

    # --------------------------------------

    # base matrix
    Matrix = [[36, 0, 0, 0],
              [0, 24, 0, 0],
              [0, 0, 19, 0],
              [0, 0, 0, 14]]




    res = 999
    iters = 0
    solRelaxed = 0
    solInteger = 0

    cs_master = CuttingStock_master(items, l, n, L, Matrix)
    #inizializzo il master fuori dal ciclo, poi il modello verrà aggiornato dinamicamente attraverso il meccanismo
    #delle Column
    cs_master.initModel()

    print("input matrix: ", Matrix)
    while(res > 1+(10**(-6))):
        myCol = Column()

        cs_slave = CuttingStock_slave(items, l, n, L)
        #risolvo il master e recupero le variabili duali
        solRelaxed = cs_master.resolve()
        u = cs_master.getDualVariables()
        print("var duali: ", u)
        #setto le variabili duali nello slave
        cs_slave.setU(u);

        #risolvo lo slave
        res = cs_slave.resolve()
        #print("--------> res slave: ", res)

        #Estraggo la nuova colonna da aggiungere ai vincoli
        new_col = cs_slave.getNewCol()
        #print("----------> new col:", new_col)
        #print("constraint: ", cs_master.r.getConstrs())

        #aggiorno anche la matrice per questioni di gestione, ma non sarebbe necessario
        Matrix.append(new_col)

        myCol.addTerms(new_col, cs_master.m.getConstrs())
        cs_master.m.update()
        #creo la nuova variabile e la aggancio ai nuovi vincoli
        cs_master.m.addVar(vtype=GRB.INTEGER, name="y", column=myCol, obj=1)
        #tengo traccia dell'iterazione appena svolta
        iters += 1

    #risolvo il master con lo standard GUROBI MIP solver, il modello contiene già i nuovi pattern aggiunti
    cs_master.m.optimize()
    solInteger = cs_master.m.getObjective().getValue()

    #salvo il tutto
    cs_master.m.write("MasterInteger.lp")
    cs_master.r.write("MasterRelaxed.lp")
    cs_slave.m.write("CS_subProblem.lp")


    #stampa dei risultati
    print()
    print()
    print("==========================================================")
    print("-----------------Problema RILASSATO-----------")
    if cs_master.r.status == GRB.OPTIMAL:
        for j in range(len(Matrix)-1):
            if(cs_master.r.getVars()[j].x > 0):
                print("Pattern utilizzato: ", Matrix[j], ", nUtilizzi: ",cs_master.r.getVars()[j].x)
    print()
    print("valore FO  del problema rilassato: ", solRelaxed)
    print("Iterazioni tra master-column generator effettuate: ", iters)
    print()
    print("-----------------Problema INTERO-----------")

    if cs_master.m.status == GRB.OPTIMAL:
        for j in range(len(Matrix)-1):
            if(cs_master.m.getVars()[j].x > 0):
                print("Pattern utilizzato: ", Matrix[j], ", nUtilizzi: ", cs_master.m.getVars()[j].x)

    print("valore ottimo del problema intero con general Gurobi MIP solver: ", solInteger)


    print("==========================================================")


    print("------- Matrice ------------")
    print(Matrix)