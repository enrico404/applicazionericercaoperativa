from gurobipy import *


class CuttingStock_slave:
      '''
      Classe che implementa il column-generator e genera le colonne di volta in volta per il master
      '''
      def __init__(self, items, l, n , L):
            self.items = items
            self.l = l
            self.n = n
            self.L = L
            self.m = Model("CS_slave")
            self.u = {}
            self.newCol = []

      #metodo che setta il valore delle variabili duali calcolate dal master
      def setU(self, u):
            self.u = u


      #metodo che va a settare il modello e va poi ad ottimizzarlo
      def resolve(self):
            if(len(self.u) != 0):
                  z = self.m.addVars(self.items, name="z", vtype=GRB.INTEGER)
                  self.m.addConstr((quicksum(self.l[i] * z[i] for i in self.items) <= self.L), name="v1")

                  self.m.setObjective(quicksum(self.u[i] * z[i] for i in self.items), GRB.MAXIMIZE)

                  self.m.optimize()
                  self.m.write("CS_subproblem.lp")

                  self.newCol = []
                  for i in self.m.getVars():
                        #print(i.varName, i.x)
                        self.newCol.append(i.x)

                  res = self.m.getObjective().getValue()
                  return res

      #metodo che ritorna la nuova colonna da aggiungere al problema master
      def getNewCol(self):
            return self.newCol