from gurobipy import *
import os
import xlrd


book = xlrd.open_workbook(os.path.join("Diet.xlsx"))
dietSheet = book.sheet_by_name("Table")

nutrients = []
foods = []
i = 2
#recupero nutrienti
while i < dietSheet.nrows-1:
    nutrients.append(dietSheet.cell_value(i, 0))
    i += 1

print("-------- NUTRIENTS --------")
print(nutrients)

i = 1
#recuper cibi
while i < dietSheet.ncols-3:
    foods.append(dietSheet.cell_value(1, i))
    i += 1

print("-------- FOODS --------")
print(foods)

#recupero costi
costs = {f: 0 for f in foods}
rowCosts = dietSheet.nrows - 1
j = 1
for f in foods:
    costs[f] = dietSheet.cell_value(rowCosts, j )
    j += 1

print("-------- Costs --------")
print(costs)

# A è la matrice contenente i valori nutrizionali
A = {(n,f): 0 for n in nutrients for f in foods }
i = 2
j = 1
for n in nutrients:
    for f in foods:
        A[(n,f)] = dietSheet.cell_value(i, j)
        j += 1
    i += 1
    j = 1

#print(A)

# leggo i requisiti
MinReq = {n: 0 for n in nutrients}
MaxReq = {n: 0 for n in nutrients}
i = 2
for n in nutrients:
    MinReq[n] = dietSheet.cell_value(i, dietSheet.ncols-3)
    MaxReq[n] = dietSheet.cell_value(i, dietSheet.ncols-2)
    i += 1

#print("MIN_REQ")
#print(MinReq)
#print("MAX_REQ")
#print(MaxReq)

# ----------------------------------------------------------------------

# Modello primale
print("---------------------------- Modello primale -----------------------------------------------------")
m = Model("Primal_Diet")

x = m.addVars(foods, vtype=GRB.CONTINUOUS, name=foods)
m.setObjective(quicksum(costs[f] *x[f] for f in foods), GRB.MINIMIZE)
m.addConstrs(quicksum(A[(n,f)] * x[f] for f in foods) >= MinReq[n] for n in nutrients)

m.optimize()
m.write("PrimalDiet.lp")
print("------------------------------------------------------------------------")
print("Optimal Cost: "+ str(m.objVal))

for i in m.getVars():
    print("%16s = %f" % (i.varName, i.x))

# ------------------------------------------------------------

print("---------------------------- Modello duale -----------------------------------------------------")

# Modello duale

m2 = Model("Dual_Diet")

u = m2.addVars(nutrients, vtype=GRB.CONTINUOUS, name=nutrients)
m2.setObjective(quicksum(MinReq[i] * u[i] for i in nutrients), GRB.MAXIMIZE)
m2.addConstrs(quicksum(A[i,j] * u[i] for i in nutrients) <= costs[j] for j in foods)

m2.optimize()
m2.write("DualDiet.lp")
print("------------------------------------------------------------------------")
print("Optimal Cost: "+ str(m2.objVal))

for i in m2.getVars():
    print("%16s = %f" % (i.varName, i.x))