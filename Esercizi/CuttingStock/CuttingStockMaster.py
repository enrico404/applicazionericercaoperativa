from gurobipy import *


#----DATI------
L = 20
items = ['a', 'b', 'c']
#lunghezze richieste
l = {'a': 5, 'b': 7, 'c': 9}
#richieste di pezzi per item
n = [15, 20, 25]

Matrix = [ [4,0,0], [0,2,0], [0,0,2] ]
items_key = range(len(items))

J = range(len(Matrix))
print(Matrix)
print("len:", len(Matrix))

m = Model("Cutting stock Master")

y = m.addVars(J, name="y", vtype=GRB.CONTINUOUS)

m.addConstrs((quicksum(y[j]*Matrix[j][i] for j in J) >= n[i] for i in items_key))

m.setObjective(y.sum(), GRB.MINIMIZE)

m.optimize()
m.write("CSC.lp")
for i in m.getVars():
      print(i.varName, i.x)