from gurobipy import *


#----DATI------
L = 20
items = ['a', 'b', 'c', 'd']
#lunghezze richieste
l = {'a': 5, 'b': 7, 'c': 9}
#richieste di pezzi per item
n = {'a': 15, 'b': 20, 'c': 25}

u = {'a': 0.25, 'b': 0.5, 'c': 0.5}
m = Model("SubProblem cutting stock")


z = m.addVars(items, name="z", vtype=GRB.INTEGER)
m.addConstr((quicksum(l[i]*z[i] for i in items) <= L), name="v1")

m.setObjective(quicksum(u[i]*z[i] for i in items), GRB.MAXIMIZE)

m.optimize()
m.write("CS_subproblem.lp")

for i in m.getVars():
      print(i.varName, i.x)
