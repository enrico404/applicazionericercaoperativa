from gurobipy import *


# ----------- DATI -------------

#nomi degli items
items = ['a', 'b', 'c', 'd']

#lunghezza degli items richiesta
l = {'a': 50, 'b': 73, 'c': 92, 'd': 121}

#quantità degli items richieste
n = [150, 200, 300, 200]

#lunghezza dei rolls
L = 1800

# --------------------------------------

#u = {'a': 0.027, 'b': 0.0405, 'c':0.051, 'd': 0.0674}
u = {'a': 0.027777777777777776, 'b': 0.04054520358868185, 'c': 0.0511559696342305, 'd': 0.06746031746031746}
m = Model("SubProblem cutting stock")


z = m.addVars(items, name="z", vtype=GRB.INTEGER)
m.addConstr((quicksum(l[i]*z[i] for i in items) <= L), name="v1")

m.setObjective(quicksum(u[i]*z[i] for i in items), GRB.MAXIMIZE)

m.optimize()
m.write("CS_subproblem.lp")

for i in m.getVars():
      print(i.varName, i.x)