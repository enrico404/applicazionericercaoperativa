from gurobipy import *
import os
import xlrd


#----DATI------
L = 20
items = ['a', 'b', 'c']
#lunghezze richieste
l = {'a': 5, 'b': 7, 'c': 9}
#richieste di pezzi per item
n = {'a': 150, 'b': 200, 'c': 250}

m = Model("Cutting Stock")

#somma delle richieste
N = int(sum(n[i] for i in items))
#più K è piccolo, meno variabili avrò -> 600*3 = 1800 variabili. Meno variabili ho, meno tempo ci metto.
K = range(int(N))
print("N: "+str(N))
print("K: "+str(K))
#quanti roll di tipo A -> 150 / (20/5) -> 37.5 = 38
# rollB = 200/ (20/7) = 100
#rollC = 250/(20/9) = 125
#roll totali = 38+100+125 = 263   -> questo è una euristica ed è un upperBound del problema e lo potrei usare come nuovo K per risparmiare tempo

x = m.addVars(K, name="x", vtype=GRB.BINARY)
y = m.addVars({(i,k): 0 for i in items for k in K}, name="y", vtype=GRB.INTEGER)

m.addConstrs(quicksum(y[i,k] for k in K)>= n[i] for i in items)
m.addConstrs((quicksum(l[i] * y[i,k] for i in items) <= L*x[k] for k in K), name="Roll")
m.setObjective(x.sum(), GRB.MINIMIZE)

m.optimize()
m.write("CSC.lp")
cnt = m.getConstrs()

