from gurobipy import *
import numpy as np

# ----------- DATI -------------

#nomi degli items
items = ['a', 'b', 'c', 'd']

#lunghezza degli items richiesta
l = {'a': 50, 'b': 73, 'c': 92, 'd': 121}

#quantità degli items richieste
n = [150, 200, 300, 200]

#lunghezza dei rolls
L = 1800

# --------------------------------------



#Matrix = np.diag([np.floor(L/l[i]) for i in items])

#matrice per ottenere l'ottimo
Matrix = [[36, 0, 0, 0],
          [0, 24,  0, 0],
          [0,  0, 19,  0],
          [0,  0,  0, 14],
          [2, 0, 0, 14],
          [0, 7, 14, 0],
          [0, 23, 0, 1]
          ]
#base matrix
# Matrix = [[36, 0, 0, 0],
#           [0, 24,  0, 0],
#           [0,  0, 19,  0],
#           [0,  0,  0, 14]]

items_key = range(len(items))

J = range(len(Matrix))
print(Matrix)
print("len:", len(Matrix))

m = Model("CS_Master")

y = m.addVars(J, name="y", vtype=GRB.INTEGER)

m.addConstrs((quicksum(y[j]*Matrix[j][i] for j in J) >= n[i] for i in items_key))

m.setObjective(quicksum(y[j] for j in J), GRB.MINIMIZE)

# r = m.relax()
# r.optimize()
#
# r.write("CSC.lp")
# for i in r.getVars():
#       print(i.varName, i.x)

m.optimize()
for i in m.getVars():
       print(i.varName, i.x)

# for c in m.getConstrs():
#      print(c, c.pi)
